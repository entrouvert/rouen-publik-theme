#! /usr/bin/env python3

import os

for dirname in sorted(os.listdir('static')):
    config = os.path.join('static', dirname, 'config.json')
    if not os.path.exists(config):
        continue
    print('static/%s/style.css' % dirname)
