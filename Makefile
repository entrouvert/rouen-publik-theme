VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="rouen-publik-theme"

prefix = /usr

all: themes.json css

themes.json: $(wildcard static/*/config.json)
	echo "[]" > themes.json

%.css: export LC_ALL=C.UTF-8
.SECONDEXPANSION:
%.css: %.scss $$(wildcard $$(@D)/*.scss)
	sassc $< $@

publik-base-theme/static/includes/_data_uris.scss: $(wildcard publik-base-theme/static/includes/img/*)
	cd publik-base-theme; python3 make_data_uris.py static/includes/

css: publik-base-theme/static/includes/_data_uris.scss $(shell python3 get_themes.py)
	rm -rf static/*/.sass-cache/

clean:
	rm -rf sdist
	rm -f static/rouen/_data_uris.scss
	rm -rf sdist publik-base-theme/static/includes/_data_uris.scss

DIST_FILES = \
	Makefile \
	publik-base-theme \
	static templates \
	get_themes.py

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/publik/themes/rouen/
	cp -r static templates themes.json $(DESTDIR)$(prefix)/share/publik/themes/rouen

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
